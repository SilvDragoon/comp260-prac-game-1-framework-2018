﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{
    public int nBees = 50;
    public float beePeriods;
    private float minBeePeriod = 1.0f;
    private float maxBeePeriod = 5.0f;
    public BeeMove beePrefab;
    public Rect spawnRect;
    public Transform atarget;

    //current time
    private float time;

    // Use this for initialization
    void Start()
    {
        // create bees
        for (int i = 0; i < nBees; i++)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);

            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;
            bee.target = atarget;
            // move the bee to a random position within 
            // the spawn rectangle
            float x = spawnRect.xMin +
                Random.value * spawnRect.width;
            float y = spawnRect.yMin +
                Random.value * spawnRect.height;

            bee.transform.position = new Vector2(x, y);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //beePeriods = Mathf.Lerp(minBeePeriod, maxBeePeriod, Random.value);
        time += Time.deltaTime;
        //counts up
        time += Time.deltaTime;

        //checks if its the right time to spawn
        if (time >= beePeriods)
        {
            spawn();
            //spawnTime = Random.Range(minBeePeriod, maxBeePeriod);

            time = 0;
            beePeriods = Random.Range(minBeePeriod, maxBeePeriod);
        }

    }

    //spawns the object and resets the time
    void spawn()
    {
        time = minBeePeriod;
        BeeMove bee = Instantiate(beePrefab);
        // attach to this object in the hierarchy
        bee.transform.parent = transform;

        // move the bee to a random position within 
        // the bounding rectangle
        float x = spawnRect.xMin + Random.value * spawnRect.width;
        float y = spawnRect.yMin + Random.value * spawnRect.height;
        bee.transform.position = new Vector2(x, y);
    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
}

