﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    //public Vector2 move;
    //public Vector2 velocity; // in metres per second
    public float maxSpeed = 5.0f;
    public float acceleration = 3.0f;
    public float brake = 5.0f; // in metres/second/second
    private float speed = 0.0f;
    public float turnSpeed = 30.0f; // in degrees/second
    private BeeSpawner beeSpawner;
    public float destroyRadius = 1.0f;

    // Use this for initialization
    void Start () {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }

        // the horizontal axis controls the turn
        float turn = Input.GetAxis("Horizontal");

        // turn the car
        //transform.Rotate(0, 0, -turn * turnSpeed * Time.deltaTime);
        transform.Rotate(0, 0, -turn * speed * turnSpeed * Time.deltaTime);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;

        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                //speed = speed - brake * Time.deltaTime;
                speed = speed - Mathf.Clamp(brake * Time.deltaTime, 0, Mathf.Abs(speed));
            }
            else
            {
                //speed = speed + brake * Time.deltaTime;
                speed = speed + Mathf.Clamp(brake * Time.deltaTime, 0, Mathf.Abs(speed));
            }
        }


        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;

        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
    }
}
